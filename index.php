<?php
@include("inc/header.php");
?>
<div class="container-fluid">
    <!-- Banner-->
    <div class="banner mb-5 p-relative">
        <img src="images/home/banner_home.png" class="m-auto d-block" width="100%">
        <h1 class="font-weight-bold p-absolute home-banner-h1">向雪懷</h1>
        <h3 class="font-weight-bold p-absolute home-banner-h2 text-uppercase"> Jolland Chan </h3>
    </div>
    <!-- -->
    <div class="container pt-5">
        <div class="jumbotron text-center">
            <h1 class="color-sand mb-5">乐坛黄金十年瞬间即逝，传奇却历久不衰… </h1>
            <div class="row">
                <div class="col-md-8 m-auto">
                    <p>向雪懷，原名陳劍和，香港著名詞作家，與多名殿堂級歌手合作，代表作《一生不變》、<br/> 《月半小夜曲》、《愛你多過愛他》等。</p>
                    <p>“我寫過上千首歌， 全都是我的寶寶，之後託付給歌手去養大，廣傳世界，流傳萬千，這 就是香港保育文化。” </p>
                </div>
            </div>
        </div>
    </div>
    <!-- -->
    <div class="abovefooter">
        <div class="container-fluid">
            <div class=" text-center " >
                <div class="row">

                    <div class="col-md-4 p-0">
                        <div class="box1">
                            <div class="moreover-container">
                                <div class="moreover m-auto">
                                    <a href="about.php" class="morelink">
                                        <h3 class="color-sand mb-3">關於向先生</h3>
                                        <hr>
                                        <p class="color-black">關鍵點 & 任職履歷 </p>
                                        <p class="tile-link color-sand">+</p>
                                        <p class="color-sand o-hide" >MORE</p>
                                    </a>
                                </div>

                            </div>
                        </div>
                    </div>

                    <div class="col-md-4 p-0">
                        <div class="box2">
                            <div class="moreover-container">
                                <div class="moreover m-auto">
                                    <a href="achievement.php" class="morelink">
                                        <h3 class="color-sand mb-3">個人成就 </h3>
                                        <hr>
                                        <p class="color-black">社會榮譽, 獲獎金曲 & 個人演唱會 </p>
                                        <p class="tile-link color-sand">+</p>
                                        <p class="color-sand o-hide" >MORE</p>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 p-0">
                        <div class="box3">
                            <div class="moreover-container">
                                <div class="moreover m-auto">
                                    <a href="gallery.php" class="morelink">
                                        <h3 class="color-sand mb-3" >影片合集</h3>
                                        <hr>
                                        <p class="color-black">相片回顧 & 金曲回放 </p>
                                        <p class="tile-link color-sand" >+</p>
                                        <p class="color-sand o-hide" >MORE</p>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
@include("inc/footer.php");
?>
