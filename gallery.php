<?php
    @include("inc/header.php");
?>
<div class="container-fluid">
<!-- Banner-->
<div class="about-banner mb-5 p-relative">
    <img src="images/gallery/banner_gallery.jpg" class="m-auto d-block" width="100%">
    <h1 class="font-weight-bold p-absolute about-banner-h1">Gallery</h1>
    <h3 class="font-weight-bold p-absolute about-banner-h2">畫廊</h3>
</div>
    <div class="container">
            <!-- Gallery-->

        <div class="popup-gallery">
            <div class ="row mb-5">
                <div class="col-md-4">
                    <a href="images/gallery/n2o000s97n0q7oq1162.jpg" ><img src="images/gallery/n2o000s97n0q7oq1162.jpg" class="m-auto d-block mresponsive" width="300" height="200"></a>
                </div>
                <div class="col-md-4">
                    <a href="images/gallery/16.08.13-headline.jpg~original.jpeg" ><img src="images/gallery/16.08.13-headline.jpg~original.jpeg" class="m-auto d-block mresponsive" width="300" height="200"></a>
                </div>
                <div class="col-md-4">
                    <a href="images/gallery/timg.jpeg" ><img src="images/gallery/timg.jpeg" class="m-auto d-block" width="300" height="200"></a>
                </div>
            </div>
            <div class="row mb-5">
                <div class="col-md-4">
                    <a href="images/gallery/16.08.13-headline.jpg~original.jpeg" ><img src="images/gallery/16.08.13-headline.jpg~original.jpeg" class="m-auto d-block mresponsive" width="300" height="200"></a>
                </div>
                <div class="col-md-4">
                    <a href="images/gallery/1471164036402_64A9924BE4E9F5FE45C157C1FA37604E.jpg" ><img src="images/gallery/1471164036402_64A9924BE4E9F5FE45C157C1FA37604E.jpg" class="m-auto d-block mresponsive" width="300" height="200"></a>
                </div>
                <div class="col-md-4">
                    <a href="images/gallery/n2o000s97n0q7oq1162.jpg" ><img src="images/gallery/n2o000s97n0q7oq1162.jpg" class="m-auto d-block" width="300" height="200"></a>
                </div>
            </div>
            <div class="row mb-5">
                <div class="col-md-4">
                    <a href="images/gallery/n2o000s97n0q7oq1162.jpg" ><img src="images/gallery/n2o000s97n0q7oq1162.jpg" class="m-auto d-block mresponsive" width="300" height="200"></a>
                </div>
                <div class="col-md-4">
                    <a href="images/gallery/16.08.13-headline.jpg~original.jpeg" ><img src="images/gallery/16.08.13-headline.jpg~original.jpeg" class="m-auto d-block mresponsive" width="300" height="200"></a>
                </div>
                <div class="col-md-4">
                    <a href="images/gallery/timg.jpeg" ><img src="images/gallery/timg.jpeg" class="m-auto d-block" width="300" height="200"></a>
                </div>
            </div>
            <div class="row mb-5">
                <div class="col-md-4">
                    <a href="images/gallery/16.08.13-headline.jpg~original.jpeg" ><img src="images/gallery/16.08.13-headline.jpg~original.jpeg" class="m-auto d-block mresponsive" width="300" height="200" class="m-auto d-block mresponsive"></a>
                </div>
                <div class="col-md-4">
                    <a href="images/gallery/1471164036402_64A9924BE4E9F5FE45C157C1FA37604E.jpg" ><img src="images/gallery/1471164036402_64A9924BE4E9F5FE45C157C1FA37604E.jpg" class="m-auto d-block mresponsive" width="300" height="200"></a>
                </div>
                <div class="col-md-4">
                    <a href="images/gallery/n2o000s97n0q7oq1162.jpg" ><img src="images/gallery/n2o000s97n0q7oq1162.jpg" class="m-auto d-block" width="300" height="200"></a>
                </div>
            </div>

        </div>



    </div>
</div>
    <script>
        $(document).ready(function() {
            $('.popup-gallery').magnificPopup({
                delegate: 'a',
                type: 'image',
                tLoading: 'Loading image #%curr%...',
                mainClass: 'mfp-img-mobile',
                gallery: {
                    enabled: true,
                    navigateByImgClick: true,
                    preload: [0,1] // Will preload 0 - before current, and 1 after the current image
                },
                image: {
                    tError: '<a href="%url%">The image #%curr%</a> could not be loaded.',
                    titleSrc: function(item) {
                        return '';
                    }
                }
            });
        });
    </script>
<?php
@include("inc/footer.php");
?>