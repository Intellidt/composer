<?php
    @include("inc/header.php");
?>
<div class="container-fluid">
<!-- Banner-->
<div class="about-banner p-relative">
    <img src="images/achievement/banner_achievement.jpg" class="m-auto d-block" width="100%">
    <h1 class="font-weight-bold p-absolute achievement-banner-h1">Achievement</h1>
    <h3 class="font-weight-bold p-absolute achievement-banner-h2">
        成就</h3>
</div>
    <div class="container">
        <!-- Social Achievments-->
        <div class="socialhonor">
                <div class="jumbotron text-center">
                    <h3 class="color-sand achievmentheading mb-5">社會榮譽</h3>
                    <div class="row">
                        <ul class="col-md-8 m-auto">
                            <li class="text-left socialachievment">向雪怀先生曾屡次荣获电台、电视台及香港作曲及作词家协会所颁发的「最佳 歌词奖」，「十大金曲奖」及「最广泛演出奖」。 </li>
                            <li class="text-left socialachievment">为中华人民共和国香港特别行政区成立二十周年主题曲《香港·我家》填词z</li>
                            <li class="text-left socialachievment">集十多年创作、制作及管理音乐行业之经验，向雪怀先生更於二零零一年创办 《e 个站》（ECD Hong Kong Limited），并兼任行政总裁，将数字音乐销售模式 首次推向全球，继续为音乐行业作出贡献。</li>
                            <li class="text-left socialachievment">2006 创办 N-Visio Music，专门提拔国内有潜质的创作人以及幕前及幕後的人 才。 </li>
                            <li class="text-left socialachievment">向雪怀先生填词的《奥运北京》，成为 2008 年北京奥运宣传活动的主题曲。 </li>
                            <li class="text-left socialachievment">推广當時在内地红极一时的《老鼠爱大米》，在 iTunes 排行榜上曾创下冲进前 二十位的辉煌战绩。 </li>
                            <li class="text-left socialachievment">曾任职宝丽金唱片有限公司唱片监制及艺员制作部主管期間，经由他负责监制 的唱片，在全球销量更超过一千万张。 </li>
                            <li class="text-left socialachievment">向雪怀先生曾多次被邀请担任本港多个大型演讲会之讲者，其中包括香港文学 节「流行歌曲歌词研讨会」，香港大学及中文大学之公开演讲等。 </li>
                            <li class="text-left socialachievment">於二零零三年，向雪怀先生更接受香港电台的邀请，担任电台节目《真音乐》 之主持，为大众分析香港流行歌曲的演变及邀请多位著名音乐创作人分享其创 作心得。 </li>
                        </ul>
                    </div>
                </div>
        </div>
    </div>
    <!-- Award-winning song-->
    <div class="awardsongs jumbotron">
            <h3 class="color-white awardsongheading mb-5 text-center">獲獎金曲</h3>
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                    <ul>
                        <li class="color-white awardsong">《雨丝.情愁》   谭咏麟 1982 年度十大中文金曲 </li>
                        <li class="color-white awardsong">《迟来的春天》   谭咏麟 1982 年度十大中文金曲</li>
                        <li class="color-white awardsong">《雨夜的浪漫》    谭咏麟 1985 年度十大中文金曲 </li>
                        <li class="color-white awardsong">《听不到的说话》   吕方 1985 年度十大中文金曲 </li>
                        <li class="color-white awardsong">《朋友》   谭咏麟 1986 年度十大中文金曲 </li>
                        <li class="color-white awardsong">《我的故事》  陈百强 1987 年度十大中文金曲 </li>
                    </ul>
                    </div>
                    <div class="col-md-6">
                    <ul>
                        <li class="color-white awardsong">《一生不變 》 李克勤 1989 年度十大中文金曲 </li>
                        <li class="color-white awardsong">《一千零一夜》  李克勤 1990 年度十大中文金曲</li>
                        <li class="color-white awardsong">《對不起我愛妳》  黎明 1991 年度十大中文金曲 </li>
                        <li class="color-white awardsong">《夏日傾情》  黎明 1993 年度十大中文金曲 </li>
                        <li class="color-white awardsong">《那有一天不想你》  黎明 1994 年度十大中文金曲</li>
                        <li class="color-white  awardsong">最佳中文(流行)歌詞獎 一人有一個夢想 (向雪懷) </li>
                    </ul>
                    </div>
                </div>
            </div>
    </div >
    <!-- Personal concert-->
    <div class="jumbotron">
        <div class="container">
            <h3 class="color-sand concert mb-5 text-center">個人演唱會</h3>
            <div class="row">
                    <div class="col-md-5">
                        <img src="images/achievement/16.08.13-headline.jpg~original.jpeg" class="img-fluid">
                    </div>
                    <div class="col-md-7">
                        <div class="">
                            <h5 class="font-weight-bold mb-3">《向雪懷一生中最愛作品演唱會》</h5>
                            <p class="text-justify">2016 年 8 月 12 日-13 日于香港红磡体育馆举办个人作品演唱会，名为《向 雪怀一生中最爱作品演唱会》，由香港旅游发展局呈献、演出业协会（香 港）有限公司统筹及策划，邓建明及叶广权担任音乐总监，美亞娱乐协办。</p>
                            <p class="text-justify"> 表演嘉宾包括谭咏麟(8 月 12 日)、周慧敏(8 月 12 日)及 钟镇涛、关淑怡、 草蜢、王馨平、林志美、黎瑞恩、汤宝如、李国祥、周启生、吴国敬、太极 乐队、张崇基、张崇德及新晋歌手陈乐基、胡琳、王梓轩、陈思彤、邝祖 德、吴幸美、姜耀哲、刘宥琳、週遊格等，星光熠熠，全场爆满，叫好叫 座！ </p>
                        </div>
                    </div>
            </div>
        </div>
    </div>
</div>
<?php
@include("inc/footer.php");
?>