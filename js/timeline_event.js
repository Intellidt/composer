var outer_div_width = $(".timeline-event:first-child").width();
$(document).ready(function () {
    var total_event_div = $(".timeline-event").length;
    var margin_left = 0;
    $(".timeline-events").width(outer_div_width * total_event_div);
    $(".timeline-event-description").addClass("invisible");

    $(".timeline-border-circle").each(function () {
        $(this).css("background-color", $(this).data("color"));
    });
    $(".timeline-border-circle").on("click", function () {
        $(".timeline-event-description").addClass("invisible");
        $(this).parent().next().removeClass("invisible");
    });
    $(".timeline-event-title").on("click", function () {
        $(this).parent().find(".timeline-border-circle").css("background-color", $(this).parent().find(".timeline-border-circle").data("color"));

    });
    var outer_container_width = $(".timeline-events-outer").outerWidth();
    $(".next-button").on("click", function () {
        margin_left += outer_div_width;
        if ((outer_container_width + margin_left) <= $(".timeline-events").outerWidth()) {
            $(".timeline-events").animate({"margin-left": -margin_left}, function () {
                displayDescription("next");
            });

        }
    });
    $(".prev-button").on("click", function () {
        var existing_margin_left = parseInt($(".timeline-events").css("margin-left"));
        if (existing_margin_left < 0 && Math.abs(existing_margin_left) >= outer_div_width) {
            $(".timeline-events").animate({"margin-left": (existing_margin_left + outer_div_width)}, function () {
                displayDescription("prev");
            });
            margin_left = 0;
        }
    });
    displayDescription("next");
});

function displayDescription(button) {
    $(".timeline-description").find(".child-div").remove();

    var reachPoint = parseInt($(".timeline-events-outer").width() / 2);
    $(".timeline-event-title").removeClass("bold");
    var selected_div = "";
    $(".timeline-event:not(.empty) .timeline-border-circle").each(function () {
        if ($(this).parents(".timeline-event").is(":visible")) {
            if ($(this).offset().left > reachPoint) {
                selected_div = $(this).parents(".timeline-event");
                return false;
            }
        } else {
            console.log("qewrewr");
        }
    });

    if (selected_div != "") {
        var div = $(".timeline-event:not(.empty):first");
        $(selected_div).find(".timeline-event-title").addClass("bold");

        var child_div = '<div class="child-div"><h2 class="timeline-year">' + $(selected_div).find(".timeline-event-title").html() + '</h2><div class="brief-description">' + $(selected_div).find(".timeline-event-description").html() + '</div></div>';
        $(".timeline-description").append(child_div);
        if (button == "next") {
            $(".child-div").css("margin-left", 1000).animate({
                "marginLeft": "0px",
                "opacity": 1
            }, "slow");
        } else {
            $(".child-div").css("margin-left", -1000).animate({
                "marginLeft": "10px",
                "opacity": 1
            }, "slow");
        }
    }
}