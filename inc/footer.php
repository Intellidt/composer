<div class="footer bg-black">
    <div class="container">
        <div class="row py-3">
            <div class="col-md-6">
                <p class="footer-text text-center mt-3">Copyright © 2019 Jollan Chan. All Rights Reserved.</p>
            </div>
            <div class="col-md-6">
            <p class="footer-text text-center mt-3">Website by &nbsp;<a href="http://intellimanagement.com" target="_blank" class="color-white">Intelli Management Group</a></p>
            </div>
        </div>
    </div>
</div>
</div>
</body>
</html>
