<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Jolland Chan</title>

    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="fontawesome/css/all.css">
    <link rel="stylesheet" href="css/magnific-popup.css">
    <link rel="stylesheet" href="css/style.css">

    <script src="js/jquery.min.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="fontawesome/js/all.js"></script>
    <script src="js/magnific-popup.js"></script>
</head>
<body>
<div class="wrapper">
    <nav class="navbar navbar-expand-lg navbar-dark">
        <div class="container">
            <img src="images/logo.png" class="navbar-brand">
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                    aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav w-100 justify-content-end">
                    <li class="nav-item">
                        <a class="nav-link <?php echo (basename($_SERVER['PHP_SELF']) == "index.php" ? "active" : "");?>" href="index.php">主頁</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link <?php echo (basename($_SERVER['PHP_SELF']) == "about.php" ? "active" : "");?>" href="about.php">關於向先生</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link <?php echo (basename($_SERVER['PHP_SELF']) == "achievement.php" ? "active" : "");?>" href="achievement.php">個人成就</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link <?php echo (basename($_SERVER['PHP_SELF']) == "gallery.php" ? "active" : "");?>" href="gallery.php">影片合集</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="https://www.weibo.com/u/1970667562?is_hot=1" traget="_blank">联系</a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
