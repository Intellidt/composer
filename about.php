<?php
@include("inc/header.php");
?>
<div class="container-fluid about-banner p-relative">
    <img src="images/about/banner_about.jpg" class="m-auto d-block " width="100%">
    <h1 class="font-weight-bold p-absolute about-banner-h1">About</h1>
    <h3 class="font-weight-bold p-absolute about-banner-h2">關於</h3>
</div>
<div class="jumbotron bg-white about-description pb-4 mb-0">
    <h3 class="text-center title">全球总销量超过一千万张的星级唱片监制</h3>
</div>
<div class="container-fluid">
    <div class="row">
        <div class="col-12 text-center mb-5 star-container"><i class="fas fa-star fa-lg mt-3"></i></div>
    </div>
    <div class="row">
        <div class="col-8 m-auto text-justify">
            <p>向雪怀，原名陈剑和，从事音乐行业超过二十年，对华语乐坛的贡献大家都有目共睹。
                向雪怀先生於音乐创作路上亦不遗馀力，其广东及国语歌词作品超过八百首；著名作品更
                数之不尽</p>
            <p class="mt-4">曾屡次荣获电台、电视台及香港作曲及作词家协会所颁发的「最佳歌词奖」，「十大金曲
                奖」及「最广泛演出奖」</p>
            <p class="mt-4">曾经负责大碟监制，歌曲填词及與多名殿堂级歌手合作，分別有徐小凤 、邓丽君、张学
                友、谭咏麟、刘德华、张国荣、李克勤、郑秀文、黄凯芹、黎明、黎瑞恩、汤宝如、彭
                羚、草蜢等等...</p>
        </div>
    </div>
    <div class="row mt-5">
        <div class="col-12">
            <div class="timeline-container">
                <div class="p-5 bg-transparent">
                    <div class="container">
                        <div class="text-center timeline-title"></div>
                        <h3 class="text-center font-weight-bold mt-3 mb-5">時間線</h3>
                        <div class="d-flex justify-content-center">
                            <div class="color-8f8f8f"><i class="fas fa-circle yellow-circle mr-3"></i>關鍵點</div>
                            <div class="ml-5 color-8f8f8f"><i class="fas fa-circle purple-circle mr-3"></i>任職履歷</div>
                        </div>
                    </div>
                    <div class="container mt-5" id="timeline-div">
                        <div class="row">
                            <div id="timeline" class="col-10 m-auto timelinr-container">
                                <ul id="dates" class="timelinr-dates">
                                    <li class="position-relative">
                                        <div class="timeline-border-circle" data-color="#a18b60"></div>
                                        <a href="#1977">1977</a>
                                    </li>
                                    <li class="position-relative">
                                        <div class="timeline-border-circle" data-color="#a18b60"></div>
                                        <a href="#1985">1985</a>
                                    </li>
                                    <li class="position-relative">
                                        <div class="timeline-border-circle" data-color="#7b6cd4"></div>
                                        <a href="#1988">1988</a>
                                    </li>
                                    <li class="position-relative">
                                        <div class="timeline-border-circle" data-color="#a18b60"></div>
                                        <a href="#1997">1997</a>
                                    </li>
                                    <li class="position-relative">
                                        <div class="timeline-border-circle" data-color="#7b6cd4"></div>
                                        <a href="#1998">1998</a>
                                    </li>
                                    <li class="position-relative">
                                        <div class="timeline-border-circle" data-color="#7b6cd4"></div>
                                        <a href="#1999">1999</a>
                                    </li>
                                    <li class="position-relative">
                                        <div class="timeline-border-circle" data-color="#a18b60"></div>
                                        <a href="#2001">2001</a>
                                    </li>
                                    <li class="position-relative">
                                        <div class="timeline-border-circle" data-color="#7b6cd4"></div>
                                        <a href="#2002">2002</a>
                                    </li>
                                    <li class="position-relative">
                                        <div class="timeline-border-circle" data-color="#7b6cd4"></div>
                                        <a href="#2003">2003</a>
                                    </li>
                                    <li class="position-relative">
                                        <div class="timeline-border-circle" data-color="#7b6cd4"></div>
                                        <a href="#2004">2004</a>
                                    </li>
                                    <li class="position-relative">
                                        <div class="timeline-border-circle" data-color="#7b6cd4"></div>
                                        <a href="#2005">2005</a>
                                    </li>
                                    <li class="position-relative">
                                        <div class="timeline-border-circle" data-color="#a18b60"></div>
                                        <a href="#2006">2006</a>
                                    </li>
                                    <li class="position-relative">
                                        <div class="timeline-border-circle" data-color="#a18b60"></div>
                                        <a href="#2008">2008</a>
                                    </li>
                                    <li class="position-relative">
                                        <div class="timeline-border-circle" data-color="#7b6cd4"></div>
                                        <a href="#2014">2014</a>
                                    </li>
                                    <li class="position-relative">
                                        <div class="timeline-border-circle" data-color="#a18b60"></div>
                                        <a href="#2016">2016</a>
                                    </li>
                                    <li class="position-relative">
                                        <div class="timeline-border-circle" data-color="#a18b60"></div>
                                        <a href="#2017">2017</a>
                                    </li>
                                </ul>
                                <ul id="issues" class="timelinr-issues mt-5 p-0">
                                    <li id="1977">
                                        <div class="description-outer m-auto">
                                            <h6 class="yellow-circle">1977</h6>
                                            <p>在宝丽金录音室工作，边念电子工文凭。得到歌神许冠杰发掘，推荐到乐坛发展</p>
                                        </div>
                                    </li>
                                    <li id="1985">
                                        <div class="description-outer m-auto">
                                            <h6 class="yellow-circle">1985</h6>
                                            <p>任职宝丽金唱片有限公司唱片监制及艺员制作部主管。为多位神级歌手如陈百强、谭咏
                                                麟、张学友、黎明、李克勤、徐小凤、黎瑞恩、彭羚、草蜢等填词、监制，名曲畅销世界。</p>
                                        </div>
                                    </li>
                                    <li id="1988">
                                        <div class="description-outer m-auto">
                                            <h6 class="purple-circle">1988 – 2000</h6>
                                            <p>香港作曲家及作词家协会作家理事</p>
                                        </div>
                                    </li>
                                    <li id="1997">
                                        <div class="description-outer m-auto">
                                            <h6 class="yellow-circle">1997</h6>
                                            <p>转到香港BMG 音乐出版有限公司担任董事总经理</p>
                                        </div>
                                    </li>
                                    <li id="1998">
                                        <div class="description-outer m-auto">
                                            <h6 class="purple-circle">1998 – 2000</h6>
                                            <p>香港音乐出版人协会理事</p>
                                        </div>
                                    </li>
                                    <li id="1999">
                                        <div class="description-outer m-auto">
                                            <h6 class="purple-circle">1999</h6>
                                            <p>健康快车会员</p>
                                        </div>
                                    </li>
                                    <li id="2001">
                                        <div class="description-outer m-auto">
                                            <h6 class="yellow-circle">2001</h6>
                                            <p>
                                                创办《e 個站》(ECD HK Ltd)，并兼任行政总裁，将数位音乐销售模式首次推向全球
                                            </p>
                                            <h6 class="purple-circle">2001 – 2005</h6>
                                            <p>
                                                香港作曲家及作词家协会副主席
                                            </p>
                                        </div>
                                    </li>
                                    <li id="2002">
                                        <div class="description-outer m-auto">
                                            <h6 class="purple-circle">2002</h6>
                                            <p>
                                                中国建设服务基金理事
                                            </p>
                                        </div>
                                    </li>
                                    <li id="2003">
                                        <div class="description-outer m-auto">
                                            <h6 class="purple-circle">2003 – 2005</h6>
                                            <p>
                                                中国金唱片颁奖典礼评委及颁奖嘉宾
                                            </p>
                                        </div>
                                    </li>
                                    <li id="2004">
                                        <div class="description-outer m-auto">
                                            <h6 class="purple-circle">2004</h6>
                                            <p>香港中文大学校什外进修学院《流行歌曲歌词创作》讲师</p>
                                            <h6 class="purple-circle">2004 – 2006</h6>
                                            <p>中国百事音乐风云榜主席评委及颁奖嘉宾</p>
                                            <h6 class="purple-circle">2004 – 2006</h6>
                                            <p>香港特别行政区表演艺术委员会委员 ( 财务小组召集人 )</p>
                                        </div>
                                    </li>
                                    <li id="2005">
                                        <div class="description-outer m-auto">
                                            <h6 class="purple-circle">2005</h6>
                                            <p>中国南方都市报传媒大奖主席评委</p>
                                            <h6 class="purple-circle">2005</h6>
                                            <p>21CN 全国网络歌手大赛主席评委</p>
                                            <h6 class="purple-circle">2005 – 2006</h6>
                                            <p>华语音乐传媒大奖主席评委及颁奖嘉宾</p>
                                            <h6 class="purple-circle">2005 – 2006</h6>
                                            <p>
                                                香港影视娱乐业技能提升计划委员
                                            </p>
                                        </div>
                                    </li>
                                    <li id="2006">
                                        <div class="description-outer m-auto">
                                            <h6 class="yellow-circle">2006</h6>
                                            <p>
                                                创办N-Visio Music，专门提拔国内有潜质的创作人以及幕前及幕后的人才
                                            </p>
                                        </div>
                                    </li>
                                    <li id="2008">
                                        <div class="description-outer m-auto">
                                            <h6 class="yellow-circle">2008</h6>
                                            <p>
                                                北京奥运宣传活动主题曲《奥运北京》亦是向老师的作品
                                            </p>
                                        </div>
                                    </li>
                                    <li id="2014">
                                        <div class="description-outer m-auto">
                                            <h6 class="purple-circle">2014</h6>
                                            <p>
                                                香港作曲家及作詞家協會《音樂成就大獎》
                                            </p>
                                        </div>
                                    </li>
                                    <li id="2016">
                                        <div class="description-outer m-auto">
                                            <h6 class="yellow-circle">2016</h6>
                                            <p>
                                                香港作曲家及作詞家協會《音樂成就大獎》
                                            </p>
                                        </div>
                                    </li>
                                    <li id="2017">
                                        <div class="description-outer m-auto">
                                            <h6 class="yellow-circle">2017</h6>
                                            <p>
                                                为香港特别行政区成立二十周年主题曲《香港·我家》填词
                                            </p>
                                        </div>
                                    </li>
                                </ul>
                                <a href="#" id="next" class="timelinr-next"><i
                                            class="fas fa-chevron-circle-right fa-2x"></i></a>
                                <a href="#" id="prev" class="timelinr-prev"><i
                                            class="fas fa-chevron-circle-left fa-2x"></i></a>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
<script src="js/jquery.timelinr-0.9.52.js"></script>
<script type="text/javascript">
    $(function () {
        $(".timeline-border-circle").each(function () {
            $(this).css("background-color", $(this).data("color"));
        });
        $("#timeline").timelinr({
            arrowKeys: 'true'
        });
    });
</script>
<?php
@include("inc/footer.php");
?>
